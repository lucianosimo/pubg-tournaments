﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class DownloadManager : MonoBehaviour {

    [System.Serializable]
    public class Attributes {
        public string createdAt;
    }
    

    [System.Serializable]
    public class Tournament {
        public string type;
        public string id;
        public Attributes attributes;
    }

    [System.Serializable]
    public class DataCollection {
        public Tournament[] data;
    } 
    
    [SerializeField] private string apiUrl = "https://api.pubg.com/tournaments";

    [Header("Prefabs")]
    [SerializeField] private GameObject tournamentWidgetPrefab = null;

    [Header("References")]
    [SerializeField] private GameObject loadingOverlay = null;
    [SerializeField] private Transform scrollContent = null;
    [SerializeField] private TextMeshProUGUI getDataButtonLabel = null;


    private const string PUBG_API_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlZDZlZjkxMC0zNGJlLTAxM2EtZGViOS00NTZkNzI5YmRlMmUiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNjM4MzU0MzI4LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6Ii0xYWE0NmVhNS1jNDM5LTQ0ZGUtYWYzYS02YmZlNTgwZGE2ZTIifQ.Oo2GK7OpCKsNPE7YOjRg7PH7fzDHZTHXoEqdLFfRjk4";
    private DataCollection retrievedData = new DataCollection();

    public void getRemoteData() {
        loadingOverlay.SetActive(true);
        initializeElements();
        StartCoroutine(getData());
    }

    private void initializeElements() {
        retrievedData = new DataCollection();
        
        foreach(Transform child in scrollContent) {
            Destroy(child.gameObject);
        }
    }

    private IEnumerator getData() {
        using (UnityWebRequest request = UnityWebRequest.Get(apiUrl)) {

            request.SetRequestHeader("Accept", "application/vnd.api+json");
            request.SetRequestHeader("Authorization", "Bearer " + PUBG_API_KEY);
            
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success) {
                Debug.Log(request.error);
                loadingOverlay.SetActive(false);
            } else {
                retrievedData = JsonUtility.FromJson<DataCollection>(request.downloadHandler.text);
                Debug.Log(JsonUtility.ToJson(retrievedData));
                instantiateWidgets();
                updateUI();
            }
        }
    }

    private void instantiateWidgets() {
        foreach (Tournament t in retrievedData.data) {
            GameObject tournamentWidget = Instantiate(tournamentWidgetPrefab, scrollContent);
            tournamentWidget.transform.Find("IDInfo").GetComponent<TextMeshProUGUI>().text = t.id;
            
            try {
                tournamentWidget.transform.Find("CreatedDate").GetComponent<TextMeshProUGUI>().text = DateTime.Parse(t.attributes.createdAt).ToShortDateString();
            } catch (Exception e) {
                tournamentWidget.transform.Find("CreatedDate").GetComponent<TextMeshProUGUI>().text = t.attributes.createdAt;
				UnityEngine.Debug.LogErrorFormat("Error to parse  %s", t.attributes.createdAt);
            }
        }
    }

    private void updateUI() {
        getDataButtonLabel.text = "RELOAD DATA";
        loadingOverlay.SetActive(false);
    }
}
